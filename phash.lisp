(in-package #:phash)

(defun make-persistent-hash-table (directory &key (hashing-depth 2) (fsync nil) (fast-index t))
  "Creates a persistent hash table.

DIRECTORY specifies in which directory the values of the hash table should be
persisted.

HASHING-DEPTH specifies how much depth in directories each value should be
persisted.

FSYNC specifies whether the files and directories should be fsync'ed after each
write. If set to T, the data is more durable, but each write has an impact on
performance. Note that if FAST-INDEX is also set to T, then the index is
fsync'ed on each write as well.

FAST-INDEX specifies whether an index of the keys should be written to disk and
kept in-memory. If set to NIL, the index is not kept in memory nor written to
disk, but ITER-ITEMS, TABLE-KEYS or TABLE-VALUES are going to traverse the
directories."
  (check-type fsync boolean)
  (check-type fast-index boolean)
  (assert (< hashing-depth 10))
  (assert (null (pathname-name (ensure-directories-exist directory))))
  (make-instance 'phash
                 :path directory
                 :hashing-depth hashing-depth
                 :fsync fsync
                 :index-path (when fast-index
                               (merge-pathnames #p"index" directory))))

(defclass phash ()
  ((path :initarg :path :type pathname :reader phash-path)
   (hashing-depth :initarg :hashing-depth :type integer :reader hashing-depth)
   (fsync :initarg :fsync :type boolean :reader fsync)
   (index-path :initarg :index-path :reader index-path)
   (index :initform nil :accessor index)))

(defmethod initialize-instance :after ((h phash) &key)
  (when (fast-index h)
    (setf (index h)
          (handler-case
              (cl-store:restore (index-path h))
            (error ()
              (make-hash-table))))))

(defun fast-index (phash)
  (not (null (index-path phash))))

(defun store-item (phash key value)
  "Stores an item on disk."
  (let* ((hash (sxhash key))
         (path (ensure-directories-exist (key-path phash hash))))
    (when (fast-index phash)
      (multiple-value-bind (dummy presentp)
          (gethash hash (index phash))
        (declare (ignore dummy))
        (unless presentp
          (setf (gethash hash (index phash)) key)
          (cl-store:store (index phash) (index-path phash)))))

    (cl-store:store key (merge-pathnames #p"key" path))
    (cl-store:store value (merge-pathnames #p"value" path))

    ;; TODO: fsync index, file, directories
    value))

(defun key-path (phash hash)
  (let ((hstr (write-to-string hash :base 16)))
    (merge-pathnames (make-pathname
                      :directory (append '(:relative)
                                         (loop for i from 0 below (hashing-depth phash)
                                               collect (subseq hstr i (1+ i)))
                                         (list (subseq hstr (hashing-depth phash)))))
                     (phash-path phash))))

(defun fetch-item (phash key)
  "Fetches the item for KEY from disk.

Returns 2 values: the first one is either the value on disk, or NIL. The second
value is a boolean indicating whether the key existed on disk."
  (handler-case
      (values (cl-store:restore (merge-pathnames #p"value" (key-path phash (sxhash key)))) t)
    (file-error ()
      (values nil nil))))

(defun delete-item (phash key)
  "Deletes the item indexed by KEY from the disk.

Returns T if there was such an entry, or NIL otherwise."
  (let ((hash (sxhash key)))
    (when (and (fast-index phash) (remhash hash (index phash)))
      (cl-store:store (index phash) (index-path phash)))

    (handler-case
        (let ((path (key-path phash hash)))
          (delete-file (merge-pathnames #p"key" path))
          (delete-file (merge-pathnames #p"value" path)))
      (file-error ()
        (return-from delete-item nil)))

    ;; TODO use rmdir to get a free "cleanup or ignore if not empty"
    t))

(defun table-keys (phash)
  "Returns a list of the persistent hash table keys.

This function does not read from the disk if FAST-INDEX is used."
  (if (fast-index phash)
      (hash-table-values (index phash))
      ;; TODO after some examples are written to disk.
      ))

(defun table-values (phash)
  "Returns a list of the persistent hash table values."
  (if (fast-index phash)
      (loop for hash being the hash-keys in (index phash)
            collect (cl-store:restore (merge-pathnames #p"value" (key-path phash hash))))
      ;; TODO after some examples are written to disk.
      ))

(defun iter-items (phash function-designator)
  "Calls FUNCTION-DESIGNATOR for each item in the persistent hash table with 2
arguments: KEY and VALUE."
  (maphash (lambda (hash key)
             (funcall function-designator
                      key
                      (cl-store:restore (merge-pathnames #p"value" (key-path phash hash)))))
           (index phash)))
