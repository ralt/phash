(push (truename #p"../") asdf:*central-registry*)

(ql:quickload :phash :silent t)
(use-package :phash)

(defvar *h* (make-persistent-hash-table #p"/tmp/phash-persistence/"))
(defvar *sentinel* #p"/tmp/phash-persistence/sentinel")
(defvar *keys* '(1 2 "foo" "bar" (1 2)))

(if (probe-file *sentinel*)
    (dolist (key *keys*)
      (assert (equal (fetch-item *h* key) key)))
    (dolist (key *keys*)
      (store-item *h* key key)))

(open *sentinel* :direction :probe :if-does-not-exist :create)

(format t "~a~%" (table-keys *h*))
(format t "~a~%" (table-values *h*))
(iter-items *h* (lambda (k v)
                  (format t "~a: ~a~%" k v)))
