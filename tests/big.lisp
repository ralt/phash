(ql:quickload :phash)
(use-package :phash)

(defvar *count* 1000)
(defvar *value* (let ((buffer (make-array (* 10 1024) :element-type '(unsigned-byte 8))))
                  (with-open-file (stream #p"/dev/urandom"
                                          :direction :input
                                          :element-type '(unsigned-byte 8))
                    (read-sequence buffer stream)
                    buffer)))
(defvar *h* (make-persistent-hash-table #p"/tmp/phash-big/"))

(defun generate-something (i)
  (store-item *h* i *value*))

(time
 (dotimes (i *count*)
   (generate-something i)))
