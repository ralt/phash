# phash

`phash` provides an easy-to-use hash table that persists itself on disk. It has
2 advantages:

- It persists over restarts of the application.
- It can be bigger than the available memory.

The primary design goals are:

- Easy to use.
- Tradeoffs are easy to understand.
- Performance doesn't matter too much.

This is not a library optimized for fast IO. It is, however, a library that is
super easy to use and does the job just fine in a lot of cases. A simple example
would be using it as the database for a blog website.

[CL-STORE](https://common-lisp.net/project/cl-store/) is used to serialize the
values to disk.

## Usage

TODO

## License

MIT.
