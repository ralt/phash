(defpackage #:phash
  (:use #:cl #:alexandria)
  (:export #:make-persistent-hash-table
           #:store-item
           #:fetch-item
           #:delete-item
           #:table-keys
           #:table-values
           #:iter-items))
