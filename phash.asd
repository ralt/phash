(defsystem "phash"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT"
  :depends-on ("alexandria" "cl-store")
  :components ((:file "package")
               (:file "phash")))
